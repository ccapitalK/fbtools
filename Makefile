CC=gcc
C_FLAGS=-Wall -Wextra -O2 -lm 
DEBUG_FLAGS=

.PHONY: all

.DEFAULT_GOAL:=all

all: bin/clock bin/visual bin/battery bin/blank

bin/clock: src/clock.c
	${CC} ${C_FLAGS} src/clock.c -o bin/clock -lfbgraph

bin/visual: src/visual.c
	${CC} ${C_FLAGS} src/visual.c -o bin/visual -lfbgraph -lpthread

bin/battery: src/battery.c
	${CC} ${C_FLAGS} src/battery.c -o bin/battery -lfbgraph

bin/blank: src/blank.c
	${CC} ${C_FLAGS} src/blank.c -o bin/blank -lfbgraph
