#include<fbgraph/graph.h>
#include<fbgraph/timer.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<ctype.h>
#include<stdint.h>

#include<sys/mman.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<time.h>
#include<math.h>

typedef struct{
    double x;
    double y;
} vec2d;

vec2d createVec(double x, double y){
    vec2d returnVec;
    returnVec.x=x;
    returnVec.y=y;
    return returnVec;
}

vec2d rotateCL(vec2d *v, double rot){
    return createVec(v->x*cos(rot) - v->y * sin(rot), v->x*sin(rot) + v->y * cos(rot));
}

vec2d rotateCCL(vec2d *v, double rot){
    return rotateCL(v,-rot);
}

vec2d scale(vec2d * v, double scale){
    return createVec(v->x*scale, v->y*scale);
}

void drawClock(Screen screen, uint32_t x, uint32_t y, uint8_t r, uint8_t g, uint8_t b){
    drawRectFilled(screen, x, y, x+200, y+200, 0, 0, 0);
    time_t rawTime;
    struct tm * currentTime;

    time(&rawTime);
    currentTime = localtime(&rawTime);
    int tm_hour=currentTime->tm_hour%12;
    int isAfternoon=currentTime->tm_hour/12;
    int tm_min=currentTime->tm_min;
    int tm_sec=currentTime->tm_sec;
    //printf("Time: %02d:%02d:%02d %s\n", tm_hour, tm_min, tm_sec, isAfternoon? "PM": "AM");
    drawCircle(screen, x+100, y+100, 100, 5, r, g, b);
    drawCircleFilled(screen, x+100, y+100, 5, r, g, b);

    vec2d _secHb=createVec(0,75);
    vec2d secH=rotateCL(&_secHb,M_PI*tm_sec/30.0);
    vec2d _minHb=createVec(0,60);
    vec2d minH=rotateCL(&_minHb,M_PI*(tm_min/30.0+tm_sec/1800.0));
    vec2d _hourHb=createVec(0,35);
    vec2d hourH=rotateCL(&_hourHb,M_PI*(tm_hour/6.0+tm_min/360.0+tm_sec/21600.0));
    vec2d markSt=createVec(0,82);

    //draw Markings

    for(int i = 0; i < 12; ++i){
        vec2d mbeg = rotateCL(&markSt, M_PI*i/6.0);
        vec2d mend = scale(&mbeg, 1.09);
        drawLine(screen, x+100-mbeg.x, y+100-mbeg.y, x+100-mend.x, y+100-mend.y, r, g, b);
    }
    //draw Hour hand
    drawLine(screen, x+100, y+100, x+100-hourH.x, y+100-hourH.y, r, g, b);
    //draw Minute hand
    drawLine(screen, x+100, y+100, x+100-minH.x, y+100-minH.y, r, g, b);
    //draw Second hand
    drawLine(screen, x+100, y+100, x+100-secH.x, y+100-secH.y, r, g, b);

}

int main(){

    Screen foo = init_fb();
    if(foo==NULL){
        fprintf(stderr, "Error: Could not create Screen\n");
        return EXIT_FAILURE;
    }

    //drawRect(foo, 1000, 200, 1040, 240, 255, 0, 0);

    //test crosshair
    //drawLine(foo, 1600, 705, 1600, 750, 0, 255, 255);
    //drawLine(foo, 1600, 695, 1600, 650, 0, 255, 255);
    //drawLine(foo, 1605, 700, 1650, 700, 0, 255, 255);
    //drawLine(foo, 1595, 700, 1550, 700, 0, 255, 255);
    //drawLine(foo, 1605, 705, 1650, 750, 0, 255, 255);
    //drawLine(foo, 1605, 695, 1650, 650, 0, 255, 255);
    //drawLine(foo, 1595, 705, 1550, 750, 0, 255, 255);
    //drawLine(foo, 1595, 695, 1550, 650, 0, 255, 255);
    //drawLine(foo, 1600, 700, 1600, 700, 0, 255, 255);

    //drawCircle(foo, 1600, 700, 50, 5, 255, 255, 255);
    //drawCircle(foo, 1600, 25, 50, 5, 255, 255, 255);
    while(1){
        drawClock(foo, 1680, 840, 255, 255, 255);
        sleep(1);
    }
    destroy_screen(foo);
    return EXIT_SUCCESS;
}
