#include<fbgraph/graph.h>
#include<fbgraph/timer.h>
#include<stdio.h>
#include<stdint.h>
#include<time.h>
#include<unistd.h>

int main(){
    Screen masterScreen = init_fb();
    drawRectFilled(masterScreen, 1280, 0, 1920,1080, 0, 0, 0);
    destroy_screen(masterScreen);
    return EXIT_SUCCESS;
}
