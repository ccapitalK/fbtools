#include<fbgraph/graph.h>
#include<fbgraph/timer.h>
#include<stdio.h>
#include<stdint.h>
#include<errno.h>
#include<semaphore.h>
#include<pthread.h>
#include<limits.h>
#include<unistd.h>
#include<complex.h>
#include<math.h>
#include<assert.h>
#include<string.h>

//44100/10 = 4410
//#define FRAGLENGTH 4410
#define FRAGLENGTH 2048
//channel: 0=left, 1=right
int16_t buf[FRAGLENGTH][2][2] = { 0 };// [n][channel][buffer]
int n=0;
sem_t lock[2];
pthread_t renderThread;

int quit=0;

int fgetw(FILE * fp){
    int a=fgetc(fp);
    if(a==EOF){
        return EOF;
    }
    int b=fgetc(fp);
    if(b==EOF){
        return EOF;
    }
    uint16_t retval=((b<<8)+a);
    return retval;
}

void fputw(int16_t val, FILE * fp){
    fputc(val&0xffu,fp);
    fputc(val>>8,fp);
}

int roundToNextPow2(int in){
    in--;
    in|=in>>1;
    in|=in>>2;
    in|=in>>4;
    in|=in>>8;
    in|=in>>16;
    return in+1;
}

void compute_dft(double complex in[], double complex out[], int n){
    for(int i = 0; i < n; ++i){
        double sumRe=0.0;
        for(int j = 0; j < n; ++j){
            double angle = 2 * M_PI * i * j / n;
            sumRe+=cos(angle)*in[j];
        }
        out[i]+=sumRe+0.0I;
    }
}

void swapDC(double complex * a, double complex * b){
    double complex temp = *a;
    *a=*b;
    *b=temp;
}

unsigned int reverseByteOrder(unsigned int input, unsigned int numBits){
    unsigned int retVal=0;
    if(input >= (1U << numBits)){
        PARAMETER_ERROR();
    }
    for(unsigned int i = 0; i < numBits; ++i){
        retVal<<=1;
        retVal |= input&1;
        input>>=1;
    }
    return retVal;
}

void compute_fft(double complex data[], unsigned int n){
    //check n is power of two
    int found=0;
    unsigned int logN=1;
    for(unsigned int i = 2; i!=0; i<<=1, logN++){
        if(i==n){
            found=1;
            break;
        }
    }
    if(!found){
        fprintf(stderr, "Error: call to %s(..., n=%d)\n", __func__, n);
        exit(EXIT_FAILURE);
    }
    //sort by reverse byte order
    //In this case, we are doing this in place
    for(unsigned int i = 0; i < n; ++i){
        unsigned int rev = reverseByteOrder(i,logN);
        if(i<rev){
            swapDC(&data[i],&data[rev]);
        }
    }
    //Perform Danielson Lanczos fft
    unsigned int mmax = 1;
    while(n > mmax){
        unsigned int istep = 2 * mmax;
        double theta = -M_PI/mmax;
        double temp = sin(theta/2.0);
        double complex wp = -2.0*temp*temp + sin(theta)*I;
        double complex w = 1.0 + 0.0I;
        for(unsigned int m=0; m<mmax; ++m){
            for(unsigned int i=m; i<n; i+=istep){
                unsigned int j = i+mmax;
                double complex temp = w * data[j];
                data[j]=data[i]-temp;
                data[i]=data[i]+temp;
            }
            w = w * wp + w;
        }
        mmax = istep;
    }
}

void drawVisualizer(Screen screen, uint16_t x, uint16_t y, int readBuffer, double complex * leftArr[2], double complex * rightArr[2], int arrLength){
    static unsigned int flip=0;
    const int width=640;
    const int height=320;
    drawRectFilled(screen, x, y, x+width, y+320, 99, 99, 99);
    int pos = 0;
    int16_t max[width-1];
    int16_t min[width-1];
    for(uint16_t ix=0; ix<width-1; ++ix){
        max[ix]=SHRT_MIN;
        min[ix]=SHRT_MAX;
        while(pos<(ix*FRAGLENGTH)/(width-2)){
            max[ix]=MAX(max[ix],buf[pos][0][readBuffer]);
            max[ix]=MAX(max[ix],buf[pos][1][readBuffer]);
            min[ix]=MIN(min[ix],buf[pos][0][readBuffer]);
            min[ix]=MIN(min[ix],buf[pos][1][readBuffer]);
            leftArr [flip&1][pos]=buf[pos][0][readBuffer];
            rightArr[flip&1][pos]=buf[pos][1][readBuffer];
            ++pos;
        }
        //max[ix]=MIN(max[ix],0);
        //min[ix]=MAX(min[ix],0);
    }
    compute_fft( leftArr[flip&1],arrLength);
    compute_fft(rightArr[flip&1],arrLength);
    int transformRects=128;
    pos=0;
    for(uint16_t i=0; i < transformRects; ++i){

        uint16_t x0=(i*(width-2))/transformRects+2;
        uint16_t x1=((i+1)*(width-2))/transformRects+2;

        double rectAmpL=0.0f;
        double rectAmpR=0.0f;
        int numInChunk=0;

        while(pos<(transformRects*i)/(transformRects)){
            rectAmpL +=  leftArr[(flip&1)^0][pos]*0.8;
            rectAmpR += rightArr[(flip&1)^0][pos]*0.8;
            rectAmpL +=  leftArr[(flip&1)^1][pos]*0.2;
            rectAmpR += rightArr[(flip&1)^1][pos]*0.2;
            ++pos;
            ++numInChunk;
        }
        rectAmpL/=(double)numInChunk;
        rectAmpR/=(double)numInChunk;

        uint16_t thisHeightl=CLAMP(0,sqrt(fabs(rectAmpL))/(sqrt(arrLength)/2),160);
        uint16_t thisHeightr=CLAMP(0,sqrt(fabs(rectAmpR))/(sqrt(arrLength)/2),160);
        drawRectFilled(screen,x+x0,y+1,x+x1,y+1+thisHeightl, 255, 0, 0);
        drawRectFilled(screen,x+x0,y+height-1-thisHeightr,x+x1,y+height-1, 255, 0, 0);
        //drawRectFilled(screen,x+x0,y+height/2-thisHeightl,x+x1,y+height/2, 255, 0, 0);
        //drawRectFilled(screen,x+x0,y+height/2,x+x1,y+height/2+thisHeightr, 255, 0, 0);
    }
    for(uint16_t ix=0; ix<width-1; ++ix){
        int16_t minNormalized=((int)(min[ix])*height)/(2*SHRT_MAX);
        int16_t maxNormalized=((int)(max[ix])*height)/(2*SHRT_MAX);
        drawLine(screen,x+1+ix,y+height/2+minNormalized,x+1+ix,y+height/2+maxNormalized,0x00,0x0ff,0xff);
    }
    drawRect(screen, x, y, x+width, y+height, 0xff, 0xff, 0xff);
    flip++;
}

void * renderLoop(void * paramater){
    unsigned int flip=0;
    unsigned int fftArrLength=roundToNextPow2(FRAGLENGTH);
    double complex * leftArr [2];
    double complex * rightArr[2];
    leftArr [0] = malloc(sizeof(double complex)*fftArrLength);
    leftArr [1] = malloc(sizeof(double complex)*fftArrLength);
    rightArr[0] = malloc(sizeof(double complex)*fftArrLength);
    rightArr[1] = malloc(sizeof(double complex)*fftArrLength);
    Screen myScreen = init_fb();
    Screen myBuffer = init_screen(640,320);
    assert(myScreen&&myBuffer);
    int readBuffer=1;

    for(unsigned int i = 0; i < fftArrLength; ++i){
        leftArr [1][i]=0.0+0.0I;
        rightArr[1][i]=0.0+0.0I;
    }

    while(!quit){
        for(unsigned int i = 0; i < fftArrLength; ++i){
            leftArr [flip&1][i]=0.0+0.0I;
            rightArr[flip&1][i]=0.0+0.0I;
        }
        ++flip;
        sem_wait(&lock[readBuffer]);
        drawVisualizer(myBuffer, 0, 0, readBuffer, leftArr, rightArr, fftArrLength);
        blit_screen(myBuffer, myScreen, 1280, 0);
        sem_post(&lock[readBuffer]);
        readBuffer=!readBuffer;
    }
    free( leftArr[0]);
    free(rightArr[0]);
    free( leftArr[1]);
    free(rightArr[1]);
    destroy_screen(myScreen);
    return NULL;
}

void getMonitorName(char * dest){
    FILE * pactl = popen("pactl list short sinks", "r");
    if(pactl==NULL){
        ERROR("Could not open pactl");
        exit(EXIT_FAILURE);
    }
    char dataBuf[2048];
    char * monitors[32];
    monitors[0]=dataBuf;
    int nMonitors=1;
    int n = 0;
    char b;
    while((b=fgetc(pactl))!=EOF){
        dataBuf[n++]=b;
    }
    dataBuf[n]='\0';
    for(int i = 0; i < n; ++i){
        if(dataBuf[i]=='\n'&&i+1<n){
            monitors[nMonitors++]=&dataBuf[i+1];
            dataBuf[i]='\0';
        }
    }
    for(int i = 0; i < nMonitors; ++i){
        if(strstr(monitors[i],"RUNNING")!=NULL){
            strtok(monitors[i]," \t");
            char * name = strtok(NULL," \t");
            strncpy(dest,name,256);
        }
    }
    pclose(pactl);
}

void tests(){
    assert(reverseByteOrder(2,4)==4);
    assert(reverseByteOrder(4,5)==4);
    assert(reverseByteOrder(255,8)==255);
}

int main(){
    tests();
    char cmdbuf[256] = { 0 };
    char inputMonitorName[256] = { 0 };
    getMonitorName(inputMonitorName);
    if(!inputMonitorName[0]){
        ERROR("Could not find running pulse sink");
        return EXIT_FAILURE;
    }
    sprintf(cmdbuf, "pacat --raw --record --format s16le -d %s.monitor", inputMonitorName);

    FILE * inputDevice = popen(cmdbuf, "r");
    if(inputDevice==NULL){
        ERROR("Error: Couldn't popen(\"pacat ...\")");
        fprintf(stderr, "ERRNO: %d\n", errno);
        return EXIT_FAILURE;
    }
    if(sem_init(&lock[0],0,1)==-1||sem_init(&lock[1],0,1)){
        ERROR("Error: Couldn't initialize semaphores");
        fprintf(stderr, "ERRNO: %d\n", errno);
        return EXIT_FAILURE;
    }
    if(pthread_create(&renderThread, NULL, renderLoop, NULL)!=0){
        ERROR("Error: Spawn Render() thread");
        fprintf(stderr, "ERRNO: %d\n", errno);
        return EXIT_FAILURE;
    }
    int channel=0;
    int writeBuffer=0;
    sem_wait(&lock[writeBuffer]);
    for(int b=fgetw(inputDevice); b!=EOF; b=fgetw(inputDevice)){
        buf[channel?n++:n][channel][writeBuffer]=b;
        channel=!channel;
        if(n==FRAGLENGTH){
            sem_post(&lock[writeBuffer]);
            writeBuffer=!writeBuffer;
            sem_wait(&lock[writeBuffer]);
            n=0;
        }
    }
    sem_post(&lock[writeBuffer]);
    quit=1;
    pclose(inputDevice);
    pthread_cancel(renderThread);
    return EXIT_SUCCESS;
}
